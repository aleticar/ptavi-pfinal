"""proxy."""
import sys
import socketserver
import socket
import time
import json
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

tiempod = time.time()
tiempoh = time.mktime((time.gmtime()[0], time.gmtime()[1],
                       time.gmtime()[2], 0, 0, 0, 0, 0, 0))
TIMEs = int(tiempod - tiempoh)
TIMEf = time.strftime('%Y-%m-%d', time.gmtime(tiempod))

TIMEe = str(TIMEf) + " " + str(TIMEs)


class UApHandler(ContentHandler):
    """clase."""

    def __init__(self):
        """listas."""
        self.LIST = []

        self.dicc = {"server": ["name", "ip", "puerto"],
                     "database": ["path", "passwdpath"],
                     "log": ["path"]}

    def startElement(self, name, attrs):
        """start element."""
        if name in self.dicc:
            LISTA = {}
            for i in self.dicc[name]:
                LISTA[i] = attrs.get(i, "")
            self.LIST.append([name, LISTA])

    def get_tags(self):
        """dicc."""
        return self.LIST


def DoLog(tipo, SERVERr, PORTr, mensaje):
    """log."""
    fich = open(logpath, "a")
    texto = mensaje.replace("\r\n", " ")
    if tipo:
        recibido = "Recieved from:"
    else:
        recibido = "Sent to:"
    fich.write(TIMEe + " " + recibido + SERVERr + ":" + str(PORTr)
               + ":" + texto + "\n")
    fich.close()


class ProxyEchoHandler(socketserver.DatagramRequestHandler):
    """clase."""

    Register = {}

    def register2json(self):
        """" REGISTRO ."""
        with open(datapath, 'w') as fichjson:
            json.dump(self.Register, fichjson, indent=3)
            print("Guardo información del cliente en archivo .json\r\n")

    """Echo server class."""
    def handle(self):
        """manejador."""
        server = self.client_address[0]
        port = self.client_address[1]
        print(server, port)
        data = self.rfile.read()
        recibo = data.decode("utf-8")

        recibo1 = (recibo + "\r\nVia: SIP/2.0/UDP "
                   + SERVER + ":" + PORT + "\r\n ")
        recibido = TIMEe + " Recived from: " + server
        recibido += ":" + str(port) + ": " + recibo
        DoLog(True, self.client_address[0],
              self.client_address[1], str(recibido))
        print(recibido)
        Username = recibo.split(" ")[1]
        method = recibo.split(" ")[0]
        print(method)
        print(Username)
        if method == "REGISTER":
            print(Username)

            EXPIRE = recibo.split(" ")[3]
            print(EXPIRE)
            TIME = time.time()
            tiempo = TIME + int(EXPIRE)
            TimExp = time.strftime('%Y-%m-%d %H:%M:%S',
                                   time.gmtime(tiempo))
            AcTi = time.strftime('%Y-%m-%d %H:%M:%S',
                                 time.gmtime(time.time()))

            self.Register[Username] = [server,
                                       int(port), int(TIME), EXPIRE]

            self.register2json()

            if TimExp <= AcTi:

                del self.Register[Username]
                self.register2json()
                print("Damos de baja al usuario")

            respuesta = b"SIP/2.0 200 OK\r\n"
            self.wfile.write(respuesta)

            DoLog(False, self.client_address[0],
                  self.client_address[1], str(respuesta))

        if method == "INVITE":
            if Username in self.Register:
                print("hola")
                escribe2 = "SIP/2.0 100 Trying SIP/2.0 180 Ring  " \
                           "SIP/2.0 200 OK\r\n"
                DoLog(False, self.client_address[0],
                      self.client_address[1], str(escribe2))
                self.wfile.write(str.encode(escribe2))
                with open("registered.json", "r") as f:
                    print("hola")
                    dic = json.load(f)
                    print(dic)
                    clave = list(dic.keys())
                    valores = list(dic.values())
                    PORT1 = clave[1].split(":")[2]
                    IP1 = valores[1][0]
                    print("INVITE", IP1, PORT1)

                    print("entro aqui")

                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)

                    my_socket.connect((IP1, int(PORT1)))

                    my_socket.send(str.encode(recibo1, 'utf-8'))
                    print("envio")
                    DoLog(False, self.client_address[0],
                          self.client_address[1], str(recibo1))

                print("hola")
            else:
                escribee = b"SIP/2.0 404 User Not Found\r\n"
                self.wfile.write(b"SIP/2.0 404 User Not Found\r\n")
                DoLog(False, self.client_address[0],
                      self.client_address[1], str(escribee))
                self.wfile.write((escribee))

        if method == "BYE":
            with open("registered.json", "r") as f:
                print("hola")
                dic = json.load(f)
                print(dic)
                clave = list(dic.keys())
                valores = list(dic.values())
                PORT1 = clave[1].split(":")[2]
                IP1 = valores[1][0]
                print("para ACK", IP1, PORT1)

                print("entro aqui")
            escribe3 = b"SIP/2.0 200 OK\r\n"
            self.wfile.write(bytes(escribe3))
            DoLog(False, self.client_address[0],
                  self.client_address[1], str(escribe3))
            with socket.socket(socket.AF_INET,
                               socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)

                my_socket.connect((IP1, int(PORT1)))

                my_socket.send(bytes(recibo1, 'utf-8'))
                print("envio")
                DoLog(False, self.client_address[0],
                      self.client_address[1], str(recibo1))

        elif method not in("INVITE", "ACK", "BYE", "REGISTER"):
            escribe4 = b"SIP/2.0 405 Method Not Allowed\r\n"
            self.wfile.write(escribe4)
            DoLog(False, self.client_address[0],
                  self.client_address[1], str(escribe4))

        if method == "ACK":
            with open("registered.json", "r") as f:
                print("hola")
                dic = json.load(f)
                print(dic)
                clave = list(dic.keys())
                valores = list(dic.values())
                PORT1 = clave[1].split(":")[2]
                IP1 = valores[1][0]
                print("Para BYE", IP1, PORT1)

                print("entro aqui")
            print("gola")
            with socket.socket(socket.AF_INET,
                               socket.SOCK_DGRAM) as my_socket:
                my_socket.setsockopt(socket.SOL_SOCKET,
                                     socket.SO_REUSEADDR, 1)
                my_socket.connect((IP1, int(PORT1)))
                my_socket.send(bytes(recibo1, 'utf-8'))
                print("envio")
                DoLog(False, self.client_address[0],
                      self.client_address[1], str(recibo1))
        else:
            escribe5 = b"SIP/2.0 400 Bad Request\r\n"
            self.wfile.write(escribe5)
            DoLog(False, self.client_address[0],
                  self.client_address[1], str(escribe5))


if __name__ == "__main__":

    try:
        config = sys.argv[1]

    except IndexError:

        sys.exit("Usage: python3 proxy_registrar.py config")

    parser = make_parser()
    cHandler = UApHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(config))

    xml = cHandler.get_tags()
    SERVER = xml[0][1]["ip"]
    PORT = xml[0][1]["puerto"]
    datapath = xml[1][1]["path"]
    datapasswd = xml[1][1]["passwdpath"]
    logpath = xml[2][1]["path"]

    serv = socketserver.UDPServer((SERVER,
                                   int(PORT)), ProxyEchoHandler)
    print("Server MiServidorBigBang listening at port 5556...")
    escucho = "Server MiServidorBigBang listening at port 5556..."
    DoLog(True, SERVER, PORT, str(escucho))
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado proxy")
