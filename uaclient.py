"""Programa cliente que abre un socket a un servidor."""
import socket
import sys
import simplertp
import secrets
import random
import time
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import os
tiempod = time.time()
tiempoh = time.mktime((time.gmtime()[0], time.gmtime()[1],
                       time.gmtime()[2], 0, 0, 0, 0, 0, 0))
TIMEs = int(tiempod - tiempoh)
TIMEf = time.strftime('%Y-%m-%d', time.gmtime(tiempod))

TIMEe = str(TIMEf) + " " + str(TIMEs)


class UAcHandler(ContentHandler):
    """clase."""

    def __init__(self):
        """listas."""
        self.list = []

        self.diccionario = {"account": ["username", "passwd"],
                            "uaserver": ["ip", "puerto"],
                            "rtpaudio": ["puerto"],
                            "regproxy": ["ip", "puerto"],
                            "log": ["path"],
                            "audio": ["path"]}

    def startElement(self, name, attrs):
        """start element."""
        if name in self.diccionario:
            list = {}
            for i in self.diccionario[name]:
                list[i] = attrs.get(i, "")
            self.list.append([name, list])

    def get_tags(self):
        """diccionario."""
        return self.list


def DoLog(tipo, SERVERr, PORTr, mensaje):
    """listas."""
    fich = open(logpath, "a")
    texto = mensaje.replace("\r\n", " ")
    if tipo:
        recibido = "Recieved from:"
    else:
        recibido = "Sent to:"
    fich.write(TIMEe + " " + recibido + SERVERr + ":" + str(PORTr)
               + ": " + texto + "\r\n")
    fich.close()


if __name__ == "__main__":

    try:
        config = str(sys.argv[1])
        method = str(sys.argv[2])
        option = str(sys.argv[3])

        Enviando = "Starting..."
        print(Enviando)
    except IndexError:
        sys.exit(" Usage: python3 client.py config")

    parser = make_parser()
    cHandler = UAcHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(config))

xml = cHandler.get_tags()
username = xml[0][1]["username"]
print(username)
passwd = xml[0][1]["passwd"]
SERVER = xml[1][1]["ip"]
PORT = xml[1][1]["puerto"]
rtport = xml[2][1]["puerto"]
print(rtport)
regproxyip = xml[3][1]["ip"]
regproxyport = xml[3][1]["puerto"]
logpath = xml[4][1]["path"]
audiopath = xml[5][1]["path"]

if method == "REGISTER":
    Enviando = TIMEe + " " + "Sent to " + regproxyip + ":" + regproxyport + ":"
    LINE = method + ' sip:' + username
    print(Enviando, LINE)
    LINE += ":" + PORT + ' SIP/2.0\r\n' + "Expires: " + option + "\r\n"
    DoLog(False, SERVER, PORT, str(LINE))

if method == "INVITE":
    Enviando = "Sent to " + regproxyip + ":" + regproxyport + ":"
    info = "v=0\r\n"
    info += "o=" + username + " " + SERVER + "\r\n"
    info += "s=misesion\r\n"
    info += "t=0\r\n"
    info += "m=audio " + rtport + " RTP\r\n"
    line = method + " sip:" + option + " SIP/2.0\r\n"
    line += "Content-Type: application/sdp" + "\r\n"
    line += "Content-Length: " + str(
                len(info)) + "\r\n\r\n"
    LINE = line + info
    print(Enviando, LINE)
    DoLog(False, SERVER, PORT, str(LINE))
if method == "BYE":
    LINE = (method + " sip:" + option + " SIP/2.0\r\n")
    print(LINE)
    DoLog(False, SERVER, PORT, str(LINE))

print("Starting...")
escucho = "Starting..."
DoLog(True, SERVER, PORT, str(escucho))
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((regproxyip, int(regproxyport)))

    my_socket.send(bytes(LINE, 'utf-8'))
    data = my_socket.recv(1024)
    recibo = data.decode("utf-8")
    recibido = TIMEe + " Recived from: " + regproxyip
    recibido += ":" + str(regproxyport) + ": " + recibo
    DoLog(True, SERVER, PORT, str(recibo))

    method2 = sys.argv[2]

    print(recibido)

    if method2 == "INVITE":
        try:
            recibo1 = recibo.split()[0] + recibo.split()[1] + recibo.split()[2]
            recibo2 = recibo.split()[3] + recibo.split()[4] + recibo.split()[5]
            recibo3 = recibo.split()[6] + recibo.split()[7] + recibo.split()[8]

        except IndexError:
            pass
        if recibo1 == "SIP/2.0100Trying" and recibo2 == "SIP/2.0180Ring":
            if recibo3 == "SIP/2.0200OK":
                envio1 = (TIMEe + " Sent to " + regproxyip
                          + ":" + regproxyport + ":")
                envio2 = "ACK" + " sip:" + option + " SIP/2.0\r\n"
                enviando = envio1 + envio2
                DoLog(False, SERVER, PORT, str(enviando))
                print(enviando)
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((regproxyip,
                                       int(regproxyport)))

                    my_socket.send(bytes(envio2, 'utf-8'))
                VLC = "cvlc rtp://@127.0.0.1:"
                VLC += rtport + " 2> /dev/null &"
                print(rtport)
                os.system(VLC)
                print("Ejecutamos VLC", VLC)
                ramdom = random.randint(0, 1000)
                marcador = secrets.randbits(1)
                RTP_header = simplertp.RtpHeader()
                numero = random.randint(0, 15)
                csrc = [random.randint(0, 2 ** 10) for _ in range(numero)]
                RTP_header.set_header(version=2, marker=marcador,
                                      payload_type=14,
                                      ssrc=ramdom, pad_flag=0, cc=len(csrc))
                RTP_header.setCSRC(csrc)
                audiortp = simplertp.RtpPayloadMp3(audiopath)
                print("hola")
                simplertp.send_rtp_packet(RTP_header,
                                          audiortp, SERVER, int(rtport))
                print("envio")

    if method2 == "BYE":
        respuesta = "SIP/2.0 200 OK\r\n"
        enviando = (TIMEe + " Sent to " + regproxyip + ":"
                    + regproxyport + ":" + respuesta)
        DoLog(False, SERVER, PORT, str(enviando))
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((regproxyip, int(regproxyport)))

            my_socket.send(bytes(respuesta, 'utf-8'))
