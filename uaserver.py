import sys
import socketserver
import time
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import os
import simplertp
import secrets
import random

tiempod = time.time()
tiempoh = time.mktime((time.gmtime()[0], time.gmtime()[1],
                       time.gmtime()[2], 0, 0, 0, 0, 0, 0))
TIMEs = int(tiempod - tiempoh)
print(tiempod)
print(tiempoh)
print(TIMEs)
TIMEf = time.strftime('%Y-%m-%d', time.gmtime(tiempod))

TIME = str(TIMEf) + " " + str(TIMEs)


class UAsHandler(ContentHandler):
    def __init__(self):

        self.LIST = []
        self.dicc = {"account": ["username", "passwd"],
                     "uaserver": ["ip", "puerto"],
                     "rtpaudio": ["puerto"],
                     "regproxy": ["ip", "puerto"],
                     "log": ["path"],
                     "audio": ["path"]}

    def startElement(self, name, attrs):
        if name in self.dicc:
            LISTA = {}
            for i in self.dicc[name]:
                LISTA[i] = attrs.get(i, "")
            self.LIST.append([name, LISTA])

    def get_tags(self):
        return self.LIST


def DoLog(tipo, SERVERr, PORTr, mensaje):

    fich = open(logpath, "a")
    texto = mensaje.replace("\r\n", " ")
    if tipo:
        recibido = "Recieved from:"
    else:
        recibido = "Sent to:"
    fich.write(TIME + " " + recibido + SERVERr + ":" + str(PORTr)
               + ":" + texto + "\n")
    fich.close()


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""
    def handle(self):

        """Echo server class."""
        # Escribe dirección y puerto del cliente (de tupla client_address)
        data = self.rfile.read()
        recibo = data.decode("utf-8")
        print("ñaaa:", recibo)

        method = recibo.split(" ")[0]
        method2 = recibo.split(" ")[0]
        print("." + method + ".")

        if method == "REGISTER":
            recibido = TIME + " Recived from: " + regproxyip
            recibido += ":" + str(regproxyport) + ": " + recibo
            print(recibido)
            DoLog(True, self.client_address[0],
                  self.client_address[1], str(recibido))

        if method == "INVITE":
            print("Entro en el INVITE")
            recibido = TIME + " Recived from: " + regproxyip
            recibido += ":" + str(regproxyport) + ": " + recibo
            DoLog(True, self.client_address[0],
                  self.client_address[1], str(recibido))

            escribe2 = "SIP/2.0 100 Trying SIP/2.0 180 Ring  "\
                       "SIP/2.0 200 OK\r\n"

            info = "v=0" + "\r\n"
            info += "o=" + username + " " + SERVER + "\r\n"
            info += "s=misesion" + "\r\n"
            info += "t=0" + "\r\n"
            info += "m=audio " + str(rtport) + " RTP"
            escribe7 = "Content-Type: application/sdp" + "\r\n"
            escribe7 += "Content-Length: " + str(
                        len(info)) + "\r\n\r\n"
            envio = escribe2 + escribe7 + info
            self.wfile.write(str.encode(envio))
            print(recibido)

        if method2 == "ACK":
            recibido = TIME + " Recived from " + regproxyip
            recibido += ":" + str(regproxyport) + ": " + recibo
            print(recibido)
            VLC = "cvlc rtp://@127.0.0.1:"
            VLC += rtport + ' 2> /dev/null &'
            os.system(VLC)

            print("Ejecutamos VLC", VLC)
            DoLog(True, self.client_address[0],
                  self.client_address[1], str(recibo))
            ramdom = random.randint(0, 1000)
            marcador = secrets.randbits(1)
            RTP_header = simplertp.RtpHeader()
            numero = random.randint(0, 15)
            csrc = [random.randint(0, 2**10)for _ in range(numero)]
            RTP_header.set_header(version=2, marker=marcador, payload_type=14,
                                  ssrc=ramdom, pad_flag=0, cc=len(csrc))
            RTP_header.setCSRC(csrc)
            audiortp = simplertp.RtpPayloadMp3(audiopath)
            print("hola")
            simplertp.send_rtp_packet(RTP_header, audiortp,
                                      SERVER, int(rtport))
            print("envio")

        if method == "BYE":
            recibido = TIME + " Recived from " + regproxyip
            recibido += ":" + str(regproxyport) + ": " + recibo
            print(recibido)
            DoLog(True, self.client_address[0],
                  self.client_address[1], str(recibido))

        if method not in ("INVITE", "ACK", "BYE", "REGISTER"):
            print("Entro en metodo erroneo")
            recibido = TIME + " Recived from " + regproxyip
            recibido += ":" + str(regproxyport) + ": " + recibo
            DoLog(True, self.client_address[0],
                  self.client_address[1], str(recibido))
            escribe4 = b"SIP/2.0 405 Method Not Allowed\r\n"
            self.wfile.write(escribe4)
            DoLog(False, self.client_address[0],
                  self.client_address[1], str(escribe4))

        else:
            recibido = TIME + "Recived from " + SERVER
            recibido += ":" + str(PORT) + ": " + recibo
            DoLog(True, self.client_address[0],
                  self.client_address[1], str(recibido))
            escribe5 = b"SIP/2.0 400 Bad Request\r\n"
            self.wfile.write(escribe5)
            DoLog(False, self.client_address[0],
                  self.client_address[1], str(escribe5))


if __name__ == "__main__":

    try:
        config = sys.argv[1]
        print("Listenning...")

    except IndexError:
        sys.exit("Usage: python3 server.py config ")

    parser = make_parser()
    cHandler = UAsHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(config))

    xml = cHandler.get_tags()
    username = xml[0][1]["username"]
    passwd = xml[0][1]["passwd"]
    SERVER = xml[1][1]["ip"]
    print(SERVER)
    PORT = xml[1][1]["puerto"]
    print(PORT)
    rtport = xml[2][1]["puerto"]
    regproxyip = xml[3][1]["ip"]
    regproxyport = xml[3][1]["puerto"]
    logpath = xml[4][1]["path"]
    audiopath = xml[5][1]["path"]
    print(audiopath)

    serv = socketserver.UDPServer((SERVER, int(PORT)), EchoHandler)
    print("Listening")
    escucho = "Listening"
    DoLog(True, SERVER, PORT, str(escucho))
    serv.serve_forever()
